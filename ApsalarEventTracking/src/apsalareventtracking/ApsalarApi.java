/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apsalareventtracking;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONObject;

/**
 *
 * @author rohit
 */
public class ApsalarApi {
    private final static String USER_AGENT = "Mozilla/5.0";
    private final static String API_KEY = "timesinternet";
    private final static String OS = "Android";
    private final static String BASE_URL = "https://api.apsalar.com/api/v1/evt";
    private final static String APK_PACKAGE = "com.taskbucks.taskbucks";

//    public String sendToAppsalar(AppsalarRegMaster appsalarRegMaster) {
    public String sendToAppsalar(AppsalarRegMaster appsalarRegMaster) {
        JSONObject jsonObj = null;

        URL url = null;
        HttpsURLConnection httpsConn = null;
        BufferedReader bufferReader = null;
        String strUrl = "";
        String eventName = "";

        try {
            jsonObj = new JSONObject();
            jsonObj.put("DeviceID", appsalarRegMaster.getDeviceId());
            jsonObj.put("EmailId", appsalarRegMaster.getEmailId());
            jsonObj.put("Imei", appsalarRegMaster.getImei());
            jsonObj.put("ReferralCode", appsalarRegMaster.getReferalCode());
            jsonObj.put("UserID", appsalarRegMaster.getUserId());

            strUrl = BASE_URL;

            if (appsalarRegMaster.getAndroidAdvId() != null && !appsalarRegMaster.getAndroidAdvId().trim().equals("")) {
                strUrl = strUrl + "?aifa=" + appsalarRegMaster.getAndroidAdvId();
            } else {
                strUrl = strUrl + "?andi=" + appsalarRegMaster.getDeviceId();
            }

            strUrl = strUrl + "&p=" + OS + "&a=" + API_KEY + "&i=" + APK_PACKAGE + "&e=" + URLEncoder.encode(jsonObj.toString(), "UTF-8");

            if (appsalarRegMaster.getIsReferal().equals("Y")) {
                if (appsalarRegMaster.getIsFlagged().equals("Y")) {
                    strUrl = strUrl + "&n=New101R-Flagged";
                    eventName = "New101R-Flagged";
                } else {
                    if (appsalarRegMaster.getUserSource().equals("com.android.vending")) {
                        strUrl = strUrl + "&n=New101R";
                        eventName = "New101R";
                    } else {
                        strUrl = strUrl + "&n=New101R-X";
                        eventName = "New101R-X";
                    }

                }

            } else if (appsalarRegMaster.getIsReferal().equals("N")) {
                if (appsalarRegMaster.getIsFlagged().equals("Y")) {
                    strUrl = strUrl + "&n=New101-Flagged";
                    eventName = "New101-Flagged";
                } else {
                    if (appsalarRegMaster.getUserSource().equals("com.android.vending")) {
                        strUrl = strUrl + "&n=New101";
                        eventName = "New101";
                    } else {
                        strUrl = strUrl + "&n=New101-X";
                        eventName = "New101-X";
                    }

                }

            } else if (appsalarRegMaster.getIsReferal().equals("O")) {
                strUrl = strUrl + "&n=New101-DetailMatch";
                eventName = "New101-DetailMatch";
            }

            strUrl = strUrl + "&ip=182.18.171.152&ve=9.2";

            if (appsalarRegMaster.getUserCurrentVersion() >= 6.4 && !eventName.equals("")) {
                sendBranchEvent(appsalarRegMaster, eventName);
            }

//            logger.info("****Appsalar Api calling*****");
//            logger.info(strUrl);

            url = new URL(strUrl);
            httpsConn = (HttpsURLConnection) url.openConnection();

            // optional default is GET
            httpsConn.setRequestMethod("GET");
            httpsConn.setConnectTimeout(25000);
            httpsConn.setReadTimeout(25000);

            //add request header
            httpsConn.setRequestProperty("User-Agent", USER_AGENT);

            int responseCode = httpsConn.getResponseCode();

//            logger.info("Response Code::" + responseCode);

          
            bufferReader = new BufferedReader(new InputStreamReader(httpsConn.getInputStream()));
            String inputLine = "";
            StringBuffer response = new StringBuffer();

            while ((inputLine = bufferReader.readLine()) != null) {
                response.append(inputLine);
            }
            bufferReader.close();

//            logger.info("Response ::" + response);
//            logger.info("****Appsalar Api End*****\n");
            return "Y";
        } catch (Exception e) {
//            logger.error("Error Response ::", e);
            System.out.println("Exception occured in ApsalarRegEvent(sendToAppsalar) ::" + e.getMessage());
//            logger.info("****Api End*****\n");
        } finally {
            saveAppsalarTempReport(appsalarRegMaster, eventName);
        }

        return "F";
    }
    
}
