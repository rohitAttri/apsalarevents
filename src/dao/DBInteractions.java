/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author rohit
 */
public class DBInteractions {

    public static final Logger LOGS = Logger.getLogger(DBInteractions.class.getName());
    private JdbcTemplate jdbcObjLive;
    private JdbcTemplate jdbcObjReplica2;

    public void setDataSourceLive(DataSource dataSourceLive) {
        this.jdbcObjLive = new JdbcTemplate(dataSourceLive);
    }

    public void setDataSourceReplica2(DataSource dataSourceReplica2) {
        this.jdbcObjReplica2 = new JdbcTemplate(dataSourceReplica2);
    }

    public int getLastProcessedID() {
        String sql1 = "Select last_id FROM process_info WHERE id=1";
        int id = 0;
        try {
            id = jdbcObjReplica2.queryForObject(sql1, Integer.class);
        }catch(Exception e) {
            LOGS.info("getLastProcessedID: "+e.getMessage());
        }
        return id;
    }

    public List getEventData() {

        int last_id = getLastProcessedID();
        String query = "Select A.id,A.customer_id, B.email_id, B.google_ad_id, B.android_id, A.`type` "
                + "FROM branch_events A "
                + "LEFT JOIN customers B on A.customer_id=B.id "
                + "WHERE B.is_active=1 AND A.id > " + last_id;
        LOGS.info(query);
        List data = jdbcObjReplica2.queryForList(query);
        return data;
    }
    
    public int updateLastProcessedID(long rowID) {
        String upQuery = "Update process_info SET last_id="+rowID+" WHERE id=1";
        int status = 0;
        try{
            status = jdbcObjLive.update(upQuery);
        }catch(Exception e) {
            LOGS.error("updateLastProcessedID: Retrying.. "+e.getMessage());
            try{
                status = jdbcObjLive.update(upQuery);
            }catch(Exception ex) {
                LOGS.error("Second Retry:"+ex.getClass());
                status = jdbcObjLive.update(upQuery);
            }
            LOGS.error("updateLastProcessedID: After Retry: "+status);
        }
        return status;
    }
}
