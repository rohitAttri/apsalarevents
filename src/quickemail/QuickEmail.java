/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quickemail;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Logger;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author rohit
 */
public class QuickEmail {
    private static final Logger LOGS = Logger.getLogger(QuickEmail.class.getName());
    public static boolean emailValidation(String email) {
        try {
            String url = "http://api.quickemailverification.com/v1/verify?email=xxmail&apikey=05ff3e87973feb1326a99ba68d3def84064e3253b59566586f243ae86cae";
            url = url.replace("xxmail", email);
            URI uri = new URIBuilder(url).build();
               LOGS.info("URL for email validation : " + uri);
            HttpGet get = new HttpGet(uri);
            try (
                CloseableHttpClient client = HttpClientBuilder.create().build();
                CloseableHttpResponse response = client.execute(get)) {
                int responseCode = response.getStatusLine().getStatusCode();
                LOGS.info("Response Code From Email Validation API :" + responseCode);
                if (responseCode == 200) {
                    HttpEntity entity = response.getEntity();
                    String res = EntityUtils.toString(entity);
                 LOGS.info("Email validation API Response : " + res);     
                    JSONObject responseJsonObject = new JSONObject(res);
                    String responseString = responseJsonObject.get("result").toString();
                    return responseString.equals("valid");
                }
            } catch (IOException ex) {
               LOGS.info(ex.getLocalizedMessage());
            }
        } catch (URISyntaxException | JSONException e) {
            LOGS.info(e.getMessage());
        }
        return true;
    }
}
