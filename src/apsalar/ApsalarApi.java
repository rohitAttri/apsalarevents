/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apsalar;

import dao.DBInteractions;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author rohit
 */
public class ApsalarApi {
    
    public static final Logger logger = Logger.getLogger(DBInteractions.class.getName());
    private final static String API_KEY = "timesinternet";
    private final static String OS = "Android";
    private final static String BASE_URL = "https://api.apsalar.com/api/v1/evt";
    private final static String APK_PACKAGE = "free.mobile.internet.data.recharge";
    /**
     *
     * @param Result
     * @return
     */
    public String sendToAppsalar(Map Result) {
        JSONObject jsonObj = null;
        URL url = null;
        HttpsURLConnection httpsConn = null;
        BufferedReader bufferReader = null;
        String apsalarURL = "";
            int event = (int) Result.get("type");                // Event
            String finalEvent = Utility.giveRegistrationEvent(event);
            if (finalEvent.equals("")) {
                logger.info("Invalid Event: "+finalEvent);
            } else {
                try {
                    jsonObj = new JSONObject();
                    if(!Result.get("android_id").equals("")) {
                        jsonObj.put("android_id", Result.get("android_id"));
                    }
                    if(!Result.get("email_id").equals("")) {
                        jsonObj.put("email", Result.get("email_id"));
                    }
                    if(!Result.get("customer_id").equals("")) {
                        jsonObj.put("customer_id", Result.get("customer_id"));
                    }
                    apsalarURL = BASE_URL;
                    if ( Result.get("google_ad_id") != null && !Result.get("google_ad_id").equals("")) {
                        apsalarURL = apsalarURL + "?aifa=" + Result.get("google_ad_id");
                    } else {
                        apsalarURL = apsalarURL + "?andi=" + Result.get("android_id");
                    }
                    
                    apsalarURL = apsalarURL + "&p=" + OS + "&a=" + API_KEY + "&i=" + APK_PACKAGE + "&e=" + URLEncoder.encode(jsonObj.toString(), "UTF-8");
                    
                    apsalarURL = apsalarURL + "&n=" + finalEvent;
                    
                    logger.info("****Appsalar Api calling*****");
                    logger.info(apsalarURL);
                    
                    url = new URL(apsalarURL);
                    httpsConn = (HttpsURLConnection) url.openConnection();
                    
                    httpsConn.setRequestMethod("GET");
                    httpsConn.setConnectTimeout(25000);
                    httpsConn.setReadTimeout(25000);
                    int responseCode = httpsConn.getResponseCode();
                    logger.info("Response Code::" + responseCode);
                    
                    
                    bufferReader = new BufferedReader(new InputStreamReader(httpsConn.getInputStream()));
                    String inputLine = "";
                    StringBuffer response = new StringBuffer();
                    
                    while ((inputLine = bufferReader.readLine()) != null) {
                        response.append(inputLine);
                    }
                    bufferReader.close();
                    
                    logger.info("Response ::" + response);
                    logger.info("****Appsalar Api End*****\n");
                    return "Y";
                } catch (JSONException | IOException e) {
                    logger.error("Error Response ::"+e);
                    System.out.println("Exception occured in ApsalarRegEvent(sendToAppsalar) ::" + e.getMessage());
                    logger.error("****Api End*****\n");
                }
            }
        return "F";
    }
    
}
