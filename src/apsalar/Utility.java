package apsalar;

public class Utility {
    public static boolean isNullString(String inStr){
            return null==inStr||inStr.length()==0;
    }


    public static String giveRegistrationEvent(int type)
    {
        switch (type) {
            case 1:	//New Referral user register
                return "rdbk";
            case 2:	//New user register
                return "dbk";
            case 3:	//Non play store New user register
                return "odbk";
            case 4:	//Guest user login
                return "gdbk";
            default:
                return "";
        }
    }
}
