/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apsalar;

import dao.DBInteractions;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author rohit
 */
public class GetApsalarEvents {
    private static final Logger LOGS = Logger.getLogger(GetApsalarEvents.class.getName());
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        AbstractApplicationContext context
                = new ClassPathXmlApplicationContext("applicationContext.xml");

        DBInteractions db = (DBInteractions) context.getBean("dbinteraction");
        try {
            List events = db.getEventData();
            int Records = events.size();
            if (Records > 0) {
                long rowID = 0;
                for (Object map1 : events) {
                    Map map = (Map) map1;
                    rowID = Long.parseLong(map.get("id").toString());
                    ApsalarApi apsalar = new ApsalarApi();
                    String apiResponse = apsalar.sendToAppsalar(map);
                    if(apiResponse.equals("Y")) {
                        LOGS.info(apiResponse);
                    } else {
                        LOGS.info("FAIL: "+map);
                    }
                }
               int idUpdateStatus = db.updateLastProcessedID(rowID);
               LOGS.info("Id update Status Last_Updated_ID :"+rowID+"========= Status: "+idUpdateStatus);
            } else {
                LOGS.info("No Data to Process");
            }
        }catch(Exception ex) {
            LOGS.error(ex.getMessage());
        }
    }
}
