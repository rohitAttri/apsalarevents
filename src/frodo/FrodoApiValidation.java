/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package frodo;

import java.io.IOException;
import java.util.logging.Logger;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author rohit
 */
public class FrodoApiValidation {
    private static final Logger LOGS = Logger.getLogger(FrodoApiValidation.class.getName());
    
    public JSONObject checkWithFrodo(String userIds) {
//        System.out.println(userIds);
        String URL = "https://api.frodolabs.com/api/flagged_droplets/batch/?client=databack&userIdList=";
        URL = URL.concat(userIds);
        return requestFrodo(URL);
//        System.exit(0);
    }
    
     public JSONObject requestFrodo(String url) {
         JSONObject frodoData = null;
        try {
            
            LOGS.info("URL for Batch Frodo: " + url);
            HttpGet get = new HttpGet(url);
            get.addHeader("apikey", "299ca870-65cc-47f5-9bef-100f1f6d8596");
            try (
                CloseableHttpClient client = HttpClientBuilder.create().build();
                CloseableHttpResponse response = client.execute(get)) {
                int responseCode = response.getStatusLine().getStatusCode();
                LOGS.info("Response Code From Frodo:" + responseCode);
                if (responseCode == 200) {
                    HttpEntity entity = response.getEntity();
                    String res = EntityUtils.toString(entity);
                 LOGS.info("Email validation API Response : " + res);
                    frodoData = new JSONObject(res);
                    return frodoData;
                }
            } catch (IOException ex) {
               LOGS.info(ex.getLocalizedMessage());
            }
        } catch (JSONException e) {
            LOGS.info(e.getMessage());
        }
        return frodoData;
    }
}
